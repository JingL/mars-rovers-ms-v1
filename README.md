# mars-rovers-ms

## Description
The repository of mars rovers operations micro service

## Scripts

```bash
# install all the dependencies
npm install

# run the API by default in localhost:1300
npm start

# run unit testing
npm run unit-tests

# run integration testing
npm run integration-tests

# release version with following types: patch, major, minor
npm run relase:<type>

# The mars-rover service is running in
localhost:1300/rovers/operations
```
## Requirements
The project is running with node v8.10.0
