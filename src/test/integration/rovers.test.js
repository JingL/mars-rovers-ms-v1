const request = require('supertest');
const test = require('tape');

const { app, start } = require('../../lib/server');

const { SECRET } = require('../../lib/config/config');
const { InvalidSecret, InvalidContentType, MissingFieldOperations, NotEnoughInputToOperate } = require('../../lib/errors/errorCodes');
const { InvalidDimensions, OutOfBoundary, WrongDirection, WrongInstruction, CannotMoveForward } = require('../../lib/errors/errorMessages');

test('/rovers/operations', function (t) {
  const endpoint = '/rovers/operations';
  const identityHeader = 'x-mars-rovers-identity';

  t.test('with wrong secret', function (assert) {
    const expected = { error: InvalidSecret.message, success: false };

    request(app)
      .post(endpoint)
      .expect(InvalidSecret.code)
      .set(identityHeader, 'test')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidSecret');
        assert.end();
      });
  });

  t.test('with wrong content-type', function (assert) {
    const expected = { error: InvalidContentType.message, success: false };

    request(app)
      .post(endpoint)
      .expect(InvalidContentType.code)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidContentType');
        assert.end();
      });
  });

  t.test('without body', function (assert) {
    const expected = { error: MissingFieldOperations.message, success: false };

    request(app)
      .post(endpoint)
      .expect(MissingFieldOperations.code)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with MissingFieldOperations');
        assert.end();
      });
  });

  t.test('with not enough inputs to operate', function (assert) {
    const expected = { error: NotEnoughInputToOperate.message, success: false };

    request(app)
      .post(endpoint)
      .expect(NotEnoughInputToOperate.code)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 3 N'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with NotEnoughInputToOperate');
        assert.end();
      });
  });

  t.test('with invalid rectangle dimensions', function (assert) {
    const expected = { error: InvalidDimensions(5, -5), success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 -5 1 3 N LRM'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidDimensions');
        assert.end();
      });
  });

  t.test('with not enough input to operate the 2nd rover', function (assert) {
    const expected = { error: `${NotEnoughInputToOperate.message} rover 2`, success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 3 N MM 3 2'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with NotEnoughInputToOperate rover 2');
        assert.end();
      });
  });

  t.test('with rover starting position out of boundaries', function (assert) {
    const expected = { error: OutOfBoundary(1, -3), success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 -3 N LRM'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with OutOfBoundary');
        assert.end();
      });
  });

  t.test('with rover starting wrong orientation ', function (assert) {
    const expected = { error: WrongDirection(1, 3, 'K'), success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 3 K LRM'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with WrongDirection');
        assert.end();
      });
  });

  t.test('with rover with wrong instructions to operate', function (assert) {
    const expected = { error: WrongInstruction(1, 4, 'N', 'LRMX'), success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 3 N LRMX'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with WrongInstruction');
        assert.end();
      });
  });

  t.test('with rover cannot move forward due to reactangle boundaries exceed', function (assert) {
    const expected = { error: CannotMoveForward(1, 5, 'N'), success: false };

    request(app)
      .post(endpoint)
      .expect(420)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 3 N MMM'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with CannotMoveForward');
        assert.end();
      });
  });

  t.test('with instructions with empty spaces to operate rovers', function (assert) {
    const expected = '1 3 N 5 1 E';

    request(app)
      .post(endpoint)
      .expect(200)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '  5 5   1   2 N LMLMLMLMM   3   3  E   MMRMMRMRRM  '
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body.result, expected, 'should respond with the final instructions of rovers');
        assert.end();
      });
  });

  t.test('with correct instructions to operate rovers', function (assert) {
    const expected = '1 3 N 5 1 E';

    request(app)
      .post(endpoint)
      .expect(200)
      .set(identityHeader, SECRET)
      .set('Content-Type', 'application/json')
      .send({
        operations: '5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body.result, expected, 'should respond with the final instructions of rovers');
        assert.end();
      });
  });
});
