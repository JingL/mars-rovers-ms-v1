const test = require('tape');
const sinon = require('sinon');

const Check = require('../../../lib/utils/Check');

test('Check', function (t) {
  const sendStub = sinon.stub().returns();
  const res = {
    status: sinon.stub().returns({
      send: sendStub
    })
  };

  t.test('with invalidSecret', function (assert) {
    const expected = {
      error: 'invalidSecret',
      success: false
    };
    sendStub.returns(expected);

    assert.deepEqual(Check.invalidSecret(res), expected, 'should return invalidSecret error');
    assert.end();
  });

  t.test('with invalidContentType', function (assert) {
    const expected = {
      error: 'invalidContentType',
      success: false
    };
    sendStub.returns(expected);

    assert.deepEqual(Check.invalidContentType(res), expected, 'should return invalidContentType error');
    assert.end();
  });

  t.test('with missingFieldOperations', function (assert) {
    const expected = {
      error: 'missingFieldOperations',
      success: false
    };
    sendStub.returns(expected);

    assert.deepEqual(Check.missingFieldOperations(res), expected, 'should return missingFieldOperations error');
    assert.end();
  });

  t.test('with notEnoughInputToOperate', function (assert) {
    const expected = {
      error: 'notEnoughInputToOperate',
      success: false
    };
    sendStub.returns(expected);

    assert.deepEqual(Check.notEnoughInputToOperate(res), expected, 'should return notEnoughInputToOperate error');
    assert.end();
  });
})
