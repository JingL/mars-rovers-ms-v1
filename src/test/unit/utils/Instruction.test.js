const test = require('tape');

const Instruction = require('../../../lib/utils/Instruction');
const { CannotMoveForward } = require('../../../lib/errors/errorMessages');

test('Instruction - turnLeft', function (t) {
  t.test('with direcction North', function (assert) {
    assert.equal(Instruction.turnLeft('N'), 'W', 'should return West');
    assert.end();
  });

  t.test('with direcction West', function (assert) {
    assert.equal(Instruction.turnLeft('W'), 'S', 'should return South');
    assert.end();
  });

  t.test('with direcction South', function (assert) {
    assert.equal(Instruction.turnLeft('S'), 'E', 'should return East');
    assert.end();
  });

  t.test('with direcction East', function (assert) {
    assert.equal(Instruction.turnLeft('E'), 'N', 'should return North');
    assert.end();
  });
});

test('Instruction - turnRight', function (t) {
  t.test('with direction North', function (assert) {
    assert.equal(Instruction.turnRight('N'), 'E', 'should return East');
    assert.end();
  });

  t.test('with direction West', function (assert) {
    assert.equal(Instruction.turnRight('W'), 'N', 'should return North');
    assert.end();
  });

  t.test('with direction South', function (assert) {
    assert.equal(Instruction.turnRight('S'), 'W', 'should return West');
    assert.end();
  });

  t.test('with direction East', function (assert) {
    assert.equal(Instruction.turnRight('E'), 'S', 'should return South');
    assert.end();
  });
});

test('Instruction - moveForward', function (t) {
  const maxX = 2;
  const maxY = 4;

  t.test('with rover direction in North and y = maxY', function (assert) {
    const rover = {
      x: 1,
      y: 4,
      dir: 'N'
    };

    assert.throws(function() {
      Instruction.moveForward(rover, maxX, maxY),
      Error,
      CannotMoveForward
    });
    assert.end();
  });

  t.test('with rover direction in North and y < maxY', function (assert) {
    const rover = {
      x: 2,
      y: 3,
      dir: 'N'
    };
    const expected = {
      x: 2,
      y: 4,
      dir: 'N'
    };

    Instruction.moveForward(rover, maxX, maxY);
    assert.deepEqual(rover, expected, 'should return rover with 1 position more in Y axis');
    assert.end();
  });

  t.test('with rover direction in East and x > maxX', function (assert) {
    const rover = {
      x: 3,
      y: 4,
      dir: 'E'
    };

    assert.throws(function() {
      Instruction.moveForward(rover, maxX, maxY),
      Error,
      CannotMoveForward
    });
    assert.end();
  });

  t.test('with rover direction in East and x < maxX', function (assert) {
    const rover = {
      x: 1,
      y: 3,
      dir: 'E'
    };
    const expected = {
      x: 2,
      y: 3,
      dir: 'E'
    };

    Instruction.moveForward(rover, maxX, maxY);
    assert.deepEqual(rover, expected, 'should return rover with 1 position more in X axis');
    assert.end();
  });

  t.test('with rover direction in West', function (assert) {
    const rover = {
      x: 2,
      y: 3,
      dir: 'W'
    };
    const expected = {
      x: 1,
      y: 3,
      dir: 'W'
    };

    Instruction.moveForward(rover, maxX, maxY);
    assert.deepEqual(rover, expected, 'should return rover with 1 position less in X axis');
    assert.end();
  });

  t.test('with rover direction in West and x = 0', function (assert) {
    const rover = {
      x: 0,
      y: 0,
      dir: 'W'
    };

    assert.throws(function() {
      Instruction.moveForward(rover, maxX, maxY),
      Error,
      CannotMoveForward
    });
    assert.end();
  });

  t.test('with rover direction in South', function (assert) {
    const rover = {
      x: 2,
      y: 3,
      dir: 'S'
    };
    const expected = {
      x: 2,
      y: 2,
      dir: 'S'
    };

    Instruction.moveForward(rover, maxX, maxY);
    assert.deepEqual(rover, expected, 'should return rover with 1 position less in Y axis');
    assert.end();
  });

  t.test('with rover direction in South and y = 0', function (assert) {
    const rover = {
      x: 2,
      y: 0,
      dir: 'S'
    };

    assert.throws(function() {
      Instruction.moveForward(rover, maxX, maxY),
      Error,
      CannotMoveForward
    });
    assert.end();
  });
});
