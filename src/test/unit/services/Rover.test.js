const test = require('tape');

const Rover = require('../../../lib/services/Rover');
const { OutOfBoundary, WrongDirection } = require('../../../lib/errors/errorMessages');

test('Rover', function (t) {
  const maxX = 2;
  const maxY = 3;
  const x = 1;
  const y = 1;

  t.test('with RoverX < 0', function (assert) {
    const roverX = -1;
    const roverY = 2;
    const dir = 'test';

    assert.throws(function() {
      new Rover(x, y, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverX > maxX value', function (assert) {
    const roverX = 3;
    const roverY = 2;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverY < 0', function (assert) {
    const roverX = 1;
    const roverY = -1;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverY > maxY value', function (assert) {
    const roverX = 1;
    const roverY = 4;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverX < 0 and RoverY < 0', function (assert) {
    const roverX = -1;
    const roverY = -1;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverX < 0 and RoverY > maxY value', function (assert) {
    const roverX = -1;
    const roverY = 4;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverX > maxX value and RoverY < 0', function (assert) {
    const roverX = 3;
    const roverY = -1;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with RoverX > maxX value and RoverY > maxY', function (assert) {
    const roverX = 3;
    const roverY = 4;
    const dir = 'test';

    assert.throws(function() {
      new Rover(roverX, roverY, dir, maxX, maxY),
      Error,
      OutOfBoundary
    });
    assert.end();
  });

  t.test('with direction different than { North, South, East, West }', function (assert) {
    const dir = 'K';

    assert.throws(function() {
      new Rover(x, y, dir, maxX, maxY),
      Error,
      WrongDirection
    });
    assert.end();
  });

  t.test('with North direction', function (assert) {
    const dir = 'N';
    const expected = { x, y, dir };

    assert.deepEqual(
      new Rover(x, y, dir, maxX, maxY),
      expected,
      'should return a new Rover'
    );
    assert.end();
  });

  t.test('with South direction', function (assert) {
    const dir = 'S';
    const expected = { x, y, dir };

    assert.deepEqual(
      new Rover(x, y, dir, maxX, maxY),
      expected,
      'should return a new Rover'
    );
    assert.end();
  });

  t.test('with East direction', function (assert) {
    const dir = 'E';
    const expected = { x, y, dir};

    assert.deepEqual(
      new Rover(x, y, dir, maxX, maxY),
      expected,
      'should return a new Rover'
    );
    assert.end();
  });

  t.test('with West direction', function (assert) {
    const dir = 'W';
    const expected = { x, y, dir };

    assert.deepEqual(
      new Rover(x, y, dir, maxX, maxY),
      expected,
      'should return a new Rover'
    );
    assert.end();
  });

});
