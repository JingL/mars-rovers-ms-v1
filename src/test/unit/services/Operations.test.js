const test = require('tape');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

const turnLeftStub = sinon.stub().returnsThis();
const turnRightStub = sinon.stub().returnsThis();
const moveForwardStub = sinon.stub().returnsThis();
let RoverStub = sinon.stub();
let RectangleStub = sinon.stub();

const Operations = proxyquire('../../../lib/services/Operations', {
  './Rover': RoverStub,
  './Rectangle': RectangleStub,
  '../utils/Instruction': {
    turnLeft: turnLeftStub,
    turnRight: turnRightStub,
    moveForward: moveForwardStub
  },
  '../errors/errorMessages': {
    WrongInstruction: sinon.stub().returnsThis()
  },
  '../errors/errorCodes': {
    NotEnoughInputToOperate: sinon.stub().returnsThis()
  }
});

test('Operations - operateRovers', function (t) {
  t.test('with invalid new rectangle', function (assert) {
    const roversOperations = [5, -5, 0, 0, 1];

    RectangleStub.withArgs(5, -5).throws(new Error('invalidDimensions'));

    assert.throws(function () {
      Operations.operateRovers(roversOperations),
      Error,
      'InvalidDimensions'
    });
    assert.equal(RectangleStub.callCount, 1, 'should call new Rectangle once');
    assert.equal(RoverStub.callCount, 0, 'should not call new Rover');

    RectangleStub.reset();

    assert.end();
  });

  t.test('with not enough input to operate', function (assert) {
    const roversOperations = [5, 5, 0, 0, 1];

    RectangleStub.prototype.x = 5;
    RectangleStub.prototype.y = 5;

    assert.throws(function () {
      Operations.operateRovers(roversOperations),
      Error;
      NotEnoughInputToOperate
    });
    assert.equal(RectangleStub.callCount, 1, 'should call new Rectangle once');
    assert.equal(RoverStub.callCount, 0, 'should not call new Rover');

    RectangleStub.reset();

    assert.end();
  });

  t.test('with invalid new rover', function (assert) {
    const roversOperations = [5, 5, 0, 0, 1, 'LR'];

    RectangleStub.prototype.x = 5;
    RectangleStub.prototype.y = 5;
    RoverStub.withArgs(0, 0, 1).throws(new Error('invalidRover'));

    assert.throws(function () {
      Operations.operateRovers(roversOperations),
      Error;
      'invalidRover'
    });
    assert.equal(RectangleStub.callCount, 1, 'should call new Rectangle once');
    assert.equal(RoverStub.callCount, 1, 'should call new Rover');

    RectangleStub.reset();
    RoverStub.reset();

    assert.end();
  });

  t.test('with enough input to operate', function (assert) {
    const roversOperations = [5, 5, 0, 1, 'N', 'LRM'];
    const rectangle = {
      x: 5,
      y: 5
    };
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };
    const expected = '1 1 N';

    RectangleStub.prototype.x = rectangle.x;
    RectangleStub.prototype.y = rectangle.y;
    RoverStub.prototype.x = rover.x;
    RoverStub.prototype.y = rover.y;
    RoverStub.prototype.dir = rover.dir;
    turnLeftStub.withArgs('N').returns('W');
    turnRightStub.withArgs('W').returns('N');
    moveForwardStub.withArgs(rover, rectangle.x, rectangle.y).onFirstCall().returns(
      rover.x = 2
    );

    const result = Operations.operateRovers(roversOperations);

    assert.equal(result, expected, 'should return the final instructions of rovers as 1 1 N');
    assert.equal(turnLeftStub.callCount, 1, 'should call 1 times turnLeftStub');
    assert.equal(turnRightStub.callCount, 1, 'should call 1 times turnRightStub');
    assert.equal(moveForwardStub.callCount, 1, 'should call 1 times moveForwardStub');

    turnLeftStub.reset();
    turnRightStub.reset();
    moveForwardStub.reset();

    assert.end();
  });
});

test('Operations - executeInstructions', function (t) {
  const rectangle = {
    x: 2,
    y: 3
  };

  t.test('with wrong instruction in one of the rover instructions', function (assert) {
    const roverInstructions = 'KLR';
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };

    assert.throws(function () {
      Operations.executeInstructions(roverInstructions, rover, rectangle),
      Error;
      WrongInstruction
    });
    assert.equal(turnLeftStub.callCount, 0, 'should not call turnLeft');
    assert.equal(turnRightStub.callCount, 0, 'should not call turnRight');
    assert.equal(moveForwardStub.callCount, 0, 'should not call moveForward');

    assert.end();
  });

  t.test('with instruction LL', function (assert) {
    const roverInstructions = 'LL';
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };
    const expected = {
      x: 1,
      y: 1,
      dir: 'S'
    }

    turnLeftStub.withArgs('N').returns('W');
    turnLeftStub.withArgs('W').returns('S');

    Operations.executeInstructions(roverInstructions, rover, rectangle);
    assert.deepEqual(rover, expected, 'should update rover direction to South');
    assert.equal(turnLeftStub.callCount, 2, 'should call turnLeft twice');
    assert.equal(turnRightStub.callCount, 0, 'should not call turnRight');
    assert.equal(moveForwardStub.callCount, 0, 'should not call moveForward');

    turnLeftStub.reset();

    assert.end();
  });

  t.test('with instruction R', function (assert) {
    const roverInstructions = 'R';
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };
    const expected = {
      x: 1,
      y: 1,
      dir: 'E'
    }

    turnRightStub.withArgs('N').returns('E');

    Operations.executeInstructions(roverInstructions, rover, rectangle);
    assert.deepEqual(rover, expected, 'should update rover direction to South');
    assert.equal(turnLeftStub.callCount, 0, 'should not call turnLeft');
    assert.equal(turnRightStub.callCount, 1, 'should call turnRight once');
    assert.equal(moveForwardStub.callCount, 0, 'should not call moveForward');

    turnRightStub.reset();

    assert.end();
  });

  t.test('with instruction MM', function (assert) {
    const roverInstructions = 'MM';
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };
    const expected = {
      x: 1,
      y: 3,
      dir: 'N'
    }

    moveForwardStub.withArgs(rover, rectangle.x, rectangle.y)
      .onFirstCall().returns(
        rover.y = 2
      )
      .onSecondCall().returns(
        rover.y = 3
      );

    Operations.executeInstructions(roverInstructions, rover, rectangle);
    assert.deepEqual(rover, expected, 'should move forward 2 positions');
    assert.equal(turnLeftStub.callCount, 0, 'should not call turnLeft');
    assert.equal(turnRightStub.callCount, 0, 'should not call turnRight');
    assert.equal(moveForwardStub.callCount, 2, 'should call moveForward twice');

    moveForwardStub.reset();

    assert.end();
  });

  t.test('with instruction RRLM', function (assert) {
    const roverInstructions = 'RRLM';
    const rover = {
      x: 1,
      y: 1,
      dir: 'N'
    };
    const expected = {
      x: 2,
      y: 1,
      dir: 'E'
    }

    turnRightStub.withArgs('N').returns('E');
    turnRightStub.withArgs('E').returns('S');
    turnLeftStub.withArgs('S').returns('E');
    moveForwardStub.withArgs(rover, rectangle.x, rectangle.y).onFirstCall().returns(
      rover.x = 2
    );

    Operations.executeInstructions(roverInstructions, rover, rectangle);
    assert.deepEqual(rover, expected, 'should update rover direction to East and move forward 1 position');
    assert.equal(turnLeftStub.callCount, 1, 'should call turnLeft once');
    assert.equal(turnRightStub.callCount, 2, 'should call turnRight twice');
    assert.equal(moveForwardStub.callCount, 1, 'should call moveForward once');

    turnLeftStub.reset();
    turnRightStub.reset();
    moveForwardStub.reset();

    assert.end();
  });
});
