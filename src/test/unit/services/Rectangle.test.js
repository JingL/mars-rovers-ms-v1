const test = require('tape');

const Rectangle = require('../../../lib/services/Rectangle');

const { InvalidDimensions } = require('../../../lib/errors/errorMessages');

test('Rectangle', function (t) {
  t.test('with x = 0', function (assert) {
    const x = 0;
    const y = 1;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with x < 0', function (assert) {
    const x = -1;
    const y = 1;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with y = 0', function (assert) {
    const x = 1;
    const y = 0;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with y < 0', function (assert) {
    const x = 1;
    const y = -2;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with x = 0 and y = 0', function (assert) {
    const x = 0;
    const y = 0;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with x = 0 and y < 0', function (assert) {
    const x = 0;
    const y = -2;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with x < 0 and y = 0', function (assert) {
    const x = -1;
    const y = 0;
    assert.throws(function() {
      new Rectangle(x, y),
      Error,
      InvalidDimensions
    });
    assert.end();
  });

  t.test('with valid x and y dimensions', function (assert) {
    const x = 1;
    const y = 1;
    const expected = { x: 1, y: 1 };
    assert.deepEqual(
      new Rectangle(x, y),
      expected,
      'should return a new rectangle'
    );
    assert.end();
  });
});
