const test = require('tape');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

const missingFieldOperationsStub = sinon.stub().returnsThis();
const notEnoughInputToOperateStub = sinon.stub().returnsThis();
const operateRoversStub = sinon.stub().returnsThis();
const OperationsHandler = proxyquire('../../../lib/handlers/OperationsHandler', {
  '../utils/Check': {
    missingFieldOperations: missingFieldOperationsStub,
    notEnoughInputToOperate: notEnoughInputToOperateStub
  },
  '../services': {
    OperationsService: {
      operateRovers: operateRoversStub
    }
  },
  '../config/config': {
    MINIMUM_INPUTS: 6
  }
});

test('OperationsHandler', function (t) {
  t.test('with empty operations request param', async function (assert) {
    const res = {
      status: sinon.stub().returns({
        send: sinon.stub().returnsThis()
      })
    };
    const expected = {
      error: 'missingFieldOperations',
      success: false
    };

    missingFieldOperationsStub.withArgs(res).returns(expected);

    assert.deepEqual(
      await OperationsHandler.roversOperations({ operations: '' }, res),
      expected,
      'should return with missingFieldOperations'
    );
    assert.equal(missingFieldOperationsStub.callCount, 1, 'should call check missingFieldOperations once');
    assert.equal(notEnoughInputToOperateStub.callCount, 0, 'should not call check notEnoughInputToOperate');
    assert.equal(operateRoversStub.callCount, 0, 'should not call operateRovers service');

    missingFieldOperationsStub.reset();
    assert.end();
  });

  t.test('with not enough operations to operate', async function (assert) {
    const body = {
      operations: '5 5 0 1 2'
    };
    const res = {
      status: sinon.stub().returns({
        send: sinon.stub().returnsThis()
      })
    };
    const expected = {
      error: 'notEnoughInputToOperate',
      success: false
    };

    notEnoughInputToOperateStub.withArgs(res).returns(expected);

    assert.deepEqual(
      await OperationsHandler.roversOperations({ body }, res),
      expected,
      'should return with notEnoughInputToOperate'
    );
    assert.equal(missingFieldOperationsStub.callCount, 0, 'should not call check missingFieldOperations');
    assert.equal(notEnoughInputToOperateStub.callCount, 1, 'should call check notEnoughInputToOperate once');
    assert.equal(operateRoversStub.callCount, 0, 'should not call operateRovers service');

    notEnoughInputToOperateStub.reset();
    assert.end();
  });

  t.test('with operateRovers service error ', async function (assert) {
    const body = {
      operations: '5 -5 0 1 N LR'
    };
    const res = {
      status: sinon.stub().returns({
        send: sinon.stub().returnsThis()
      })
    };
    const expected = {
      error: 'operateRoversError',
      success: false
    };
    const expectedRes = {
      status: sinon.stub().returns({
        send: expected
      })
    };

    operateRoversStub.withArgs([5, -5, 0, 1, 'N', 'LR']).returns(expected);

    await OperationsHandler.roversOperations({ body }, res);
    assert.equal(missingFieldOperationsStub.callCount, 0, 'should not call check missingFieldOperations');
    assert.equal(notEnoughInputToOperateStub.callCount, 0, 'should not call check notEnoughInputToOperate');
    assert.equal(operateRoversStub.callCount, 1, 'should call operateRovers service once');

    operateRoversStub.reset();
    assert.end();
  });

  t.test('with operateRovers service result ', async function (assert) {
    const body = {
      operations: '5 5 0 1 N LR'
    };
    const res = {
      status: sinon.stub().returns({
        send: sinon.stub().returnsThis()
      })
    };
    const expected = {
      result: '1 2 N'
    };

    operateRoversStub.withArgs([5, 5, 0, 1, 'N', 'LR']).returns('1 2 N');

    await OperationsHandler.roversOperations({ body }, res);
    assert.equal(missingFieldOperationsStub.callCount, 0, 'should not call check missingFieldOperations');
    assert.equal(notEnoughInputToOperateStub.callCount, 0, 'should not call check notEnoughInputToOperate');
    assert.equal(operateRoversStub.callCount, 1, 'should call operateRovers service once');

    operateRoversStub.reset();
    assert.end();
  });
});
