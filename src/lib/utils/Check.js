const { InvalidSecret, InvalidContentType, MissingFieldOperations, NotEnoughInputToOperate } = require('../errors/errorCodes');

class Check {
  invalidSecret(res) {
    return statusWithSpecificError(res, InvalidSecret);
  }

  invalidContentType(res) {
    return statusWithSpecificError(res, InvalidContentType);
  }

  missingFieldOperations(res) {
    return statusWithSpecificError(res, MissingFieldOperations);
  }

  notEnoughInputToOperate(res) {
    return statusWithSpecificError(res, NotEnoughInputToOperate);
  }
}

const statusWithSpecificError = (res, specificError) => {
  return res.status(specificError.code).send({
    error: specificError.message,
    success: false
  });
}

module.exports = new Check();
