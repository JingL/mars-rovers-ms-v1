const { CannotMoveForward } = require('../errors/errorMessages');

class Instruction {
  turnLeft(dir) {
    if (dir === 'N') {
      return 'W';
    } else if (dir === 'W') {
      return 'S';
    } else if (dir === 'S') {
      return 'E';
    } else {
      return 'N';
    }
  }

  turnRight(dir) {
    if (dir === 'N') {
      return 'E';
    } else if (dir === 'W') {
      return 'N';
    } else if (dir === 'S') {
      return 'W';
    } else {
      return 'S';
    }
  }

  moveForward(rover, maxX, maxY) {
    if (rover.dir === 'N') {
      rover.y = moveToNorthOrEast(rover, rover.y, maxY);
    }
    else if (rover.dir === 'W') {
      rover.x = moveToSouthOrWest(rover, rover.x);
    }
    else if (rover.dir === 'S') {
      rover.y = moveToSouthOrWest(rover, rover.y);
    }
    else {
      rover.x = moveToNorthOrEast(rover, rover.x, maxX);
    }
  }
}

const moveToNorthOrEast = (rover, currentPos, maxPos) => {
  if (currentPos >= maxPos) {
    throw new Error(CannotMoveForward(rover.x, rover.y, rover.dir));
  }
  return ++currentPos;
}

const moveToSouthOrWest = (rover, currentPos) => {
  if (currentPos < 1) {
    throw new Error(CannotMoveForward(rover.x, rover.y, rover.dir));
  }
  return --currentPos;
}

module.exports = new Instruction();
