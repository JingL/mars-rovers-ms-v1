const InvalidSecret = {
  code: 401,
  message: 'Invalid Secret'
};

const InvalidContentType = {
  code: 401,
  message: 'Invalid Content Type'
};

const MissingFieldOperations = {
  code: 400,
  message: 'Missing parameter Operations'
};

const NotEnoughInputToOperate = {
  code: 400,
  message: 'Not enough inputs to operate'
};

module.exports = {
  InvalidSecret,
  InvalidContentType,
  MissingFieldOperations,
  NotEnoughInputToOperate
};
