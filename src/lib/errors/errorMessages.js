const InvalidDimensions = (x, y) => {
  return `Rectangle with invalid dimensions {${x}, ${y}}!`;
};

const OutOfBoundary = (x, y) => {
  return `Rover {x: ${x}, y: ${y}} starting position is out of boundaries!`
};

const WrongDirection = (x, y, dir) => {
  return `Rover {x: ${x}, y: ${y}, dir: ${dir}} starting direction is not in { N, S, E, W }!`
};

const WrongInstruction = (x, y, dir, instructions) => {
  return `Rover {x: ${x}, y: ${y}, dir: ${dir}} operating ${instructions} has instruction not in { L, R, M }!`
};

const CannotMoveForward = (x, y, dir) => {
  return `Rover {x: ${x}, y: ${y}, dir: ${dir}} cannot keep going to the indicated direcction due to rectangle boundaries exceed!`;
};

module.exports = {
  InvalidDimensions,
  OutOfBoundary,
  WrongDirection,
  WrongInstruction,
  CannotMoveForward
};
