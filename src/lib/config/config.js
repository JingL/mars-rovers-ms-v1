const {
  PORT = '1300',
  SECRET = 'MarsRoversOperationSecret',
  BODY_REQUEST_LIMIT = '50mb',
  MINIMUM_INPUTS = '6'
} = process.env;

module.exports = {
  PORT,
  SECRET,
  BODY_REQUEST_LIMIT,
  MINIMUM_INPUTS
};
