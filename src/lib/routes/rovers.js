const express = require('express');

const security = require('../middlewares/security');
const OperationsHandler = require('../handlers').OperationsHandler;

const router = new express.Router();

router
  .route('/operations')
  .all(security)
  .post(OperationsHandler.roversOperations);

module.exports = router;
