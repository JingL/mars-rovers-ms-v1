const express = require('express');
const bodyParser = require('body-parser');
const logger = require('logger').createLogger();

const { PORT, BODY_REQUEST_LIMIT } = require('./config/config');
const RoversRoutes = require('./routes').RoversRoutes;
const cors = require('./middlewares/cors');

const app = express();

app.use(bodyParser.json({
  limit: BODY_REQUEST_LIMIT
}));

app.use(cors);

app.get('/', function (req, res) {
  res.send('API is running');
});

app.get('/ping', function (req, res) {
  res.send('pong');
});

app.use('/rovers', RoversRoutes);

const start = function () {
  try {
    app.listen(PORT, function () {
      logger.info(`Running on the port ${PORT}`);
    });
  } catch (e) {
    logger.error(e);
    process.exit(1);
  }
}

module.exports = {
  app,
  start
};
