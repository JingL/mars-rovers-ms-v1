const logger = require('logger').createLogger();

const Check = require('../utils/Check');
const OperationsService = require('../services').OperationsService;

const { MINIMUM_INPUTS } = require('../config/config');

class OperationsHandler {
  async roversOperations({ body }, res) {
    try {
      let operationsToOperate = body ? body.operations : '';

      if (!operationsToOperate) {
        return Check.missingFieldOperations(res);
      }

      operationsToOperate = operationsToOperate.split(' ');
      operationsToOperate = operationsToOperate.filter(elem => elem !== '');

      if (operationsToOperate.length < MINIMUM_INPUTS) {
        return Check.notEnoughInputToOperate(res);
      }

      const finalPositions = OperationsService.operateRovers(operationsToOperate);

      return res.status(200).send({
        result: finalPositions
      });
    }
    catch(e) {
      logger.error(`Error while operating mars rovers by: ${e.message}`);
      return res.status(420).send({
        error: e.message,
        success: false
      });
    }
  };
}

module.exports = new OperationsHandler();
