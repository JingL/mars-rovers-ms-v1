const { InvalidDimensions } = require('../errors/errorMessages');

class Rectangle {
  constructor(x, y) {
    if (x <= 0 || y <= 0) {
      throw new Error(InvalidDimensions(x, y));
    }
    this.x = x;
    this.y = y;
  }
}

module.exports = Rectangle;
