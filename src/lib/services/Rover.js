const { WrongDirection, OutOfBoundary } = require('../errors/errorMessages');

class Rover {
  constructor(x, y, dir, maxX, maxY) {
    if (outOfBoundary(x, y, maxX, maxY)) {
      throw new Error(OutOfBoundary(x, y));
    }
    if (wrongDirection(dir)) {
      throw new Error(WrongDirection(x, y, dir));
    }
    this.x = x;
    this.y = y;
    this.dir = dir;
  }
}

const wrongDirection = (dir) => {
  if (dir !== 'N' && dir !== 'S' && dir !== 'E' && dir !== 'W') return true;
  else return false;
};

const outOfBoundary = (x, y, maxX, maxY) => {
  if (x < 0 || x > maxX || y < 0 || y > maxY) return true;
  else return false;
}

module.exports = Rover;
