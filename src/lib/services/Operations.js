const Rectangle = require('./Rectangle');
const Rover = require('./Rover');
const Instruction = require('../utils/Instruction');

const { NotEnoughInputToOperate } = require('../errors/errorCodes');
const { WrongInstruction } = require('../errors/errorMessages');

class Operations {
  operateRovers(operations) {
    const finalInstrucctions = [];
    const rectangle = new Rectangle(operations[0], operations[1]);
    const roversOperations = operations.slice(2, operations.length);

    for (let i = 0; i < roversOperations.length - 1; i += 4) {
      if (i + 4 > roversOperations.length) {
        throw new Error(`${NotEnoughInputToOperate.message} rover ${i/4 + 1}`);
      }

      const roverX = roversOperations[i];
      const roverY = roversOperations[i+1];
      const roverDir = roversOperations[i+2];
      const rover = new Rover(roverX, roverY, roverDir, rectangle.x, rectangle.y);

      this.executeInstructions(roversOperations[i+3], rover, rectangle);
      finalInstrucctions.push(rover.x, rover.y, rover.dir);
    }

    return finalInstrucctions.join(' ');
  }

  executeInstructions(roverInstructions, rover, rectangle) {
    const instructions = roverInstructions.split('');

    for (let j = 0; j < instructions.length; j++) {
      if (instructions[j] === 'L') {
        rover.dir = Instruction.turnLeft(rover.dir);
      } else if (instructions[j] === 'R') {
        rover.dir = Instruction.turnRight(rover.dir);
      } else if (instructions[j] === 'M') {
        Instruction.moveForward(rover, rectangle.x, rectangle.y);
      } else {
        throw new Error(WrongInstruction(rover.x, rover.y, rover.dir, roverInstructions));
      }
    }
  }
}

module.exports = new Operations();
